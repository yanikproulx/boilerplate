# Ciné Mobile

## Installation

1. create your database
2. copy `conf/samples/settings_local.py` to `conf/settings_local.py` and adjust to local setup
3. copy `bin/samples/start.bash` to `bin/start.bash` and adjust to local setup
3. copy `bin/samples/post_deploy.bash` to `bin/post_deploy.bash` and adjust to local setup
5. `mkvirtualenv --no-site-packages --python=/usr/bin/python3 [project]`
6. `pip install -r requirements.txt`
7. `./manage.py syncdb`
8. `./manage.py migrate`


## Cloning a project

These steps should be taken when cloning a new project

### Database creation

Setup either a PostreSQL or MySQL database

If django-extensions are installed, this command will print the SQL script to run:

```
./manage.py sqlcreate
```

#### PostgreSQL

```
sudo su - postgres
createuser -D -A -P [username]
createdb -O [username] [dbname]
```

#### MySQL

```
mysql -uroot -p

CREATE DATABASE [dbname] DEFAULT CHARACTER SET utf8;
CREATE USER '[username]'@'localhost' IDENTIFIED BY '[password]';
GRANT ALL PRIVILEGES ON [dbname].* TO '[username]'@'localhost' WITH GRANT OPTION;
```


### Supervisord and Nginx

Sample config files are found here:

- `docs/supervisor.conf.sample`
- `docs/nginx.conf.sample`


## Creating a new project

This should only be done when creating a project, not when cloning an existing one.

### NPM

npm files are installed localy under `node_modules` base on the content of `package.json`

You can reinstall the files by:

`npm install`

### Bower

To update files, simply:

- `./node_modules/bower/bin/bower install`

Note that static files will be overwritten.


## Deploying code ##

Make sure ansible is running on your system

```
sudo apt-get install ansible
```

From the `deployment` folder:

```
ansible-playbook -i ./inventories/stage.yml deploy.yml
ansible-playbook -i ./inventories/production.yml deploy.yml
```
