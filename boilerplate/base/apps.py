from django.apps import AppConfig


class BaseConfig(AppConfig):
	name = 'boilerplate.base'

	def ready(self):
		import boilerplate.base.signals
