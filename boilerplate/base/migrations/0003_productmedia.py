# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-29 17:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0001_initial'),
        ('base', '0002_product_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductMedia',
            fields=[
                ('media_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='gallery.Media')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='media', to='base.Product', verbose_name='Product')),
            ],
            options={
                'ordering': ['position', '-created'],
                'verbose_name_plural': 'Product Media',
                'verbose_name': 'Product Media',
            },
            bases=('gallery.media',),
        ),
    ]
