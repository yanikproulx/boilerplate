"""boilerplate URL Configuration"""

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.views.generic.base import TemplateView
from django.conf.urls.static import static

from lemoulin_extras.pages.views import StaticPageView

from boilerplate.base.views import Home


# Locale independent URLs
urlpatterns = [
	url(r'^admin/', include(admin.site.urls)),
	url(r'^grappelli/', include('grappelli.urls')),

	url(r'^lemoulin_extras/', include("lemoulin_extras.urls", namespace="lemoulin_extras")),

	url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
	url(r'^humans\.txt$', TemplateView.as_view(template_name='humans.txt', content_type='text/plain')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += i18n_patterns(
	url(r'^$', Home.as_view(), name="home"),
	url(r'^about/$', StaticPageView.as_view(), {"slug": "about", }, name="about"),
)
