"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
	GRAPPELLI_INDEX_DASHBOARD = 'laser.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from grappelli.dashboard import modules, Dashboard


class CustomIndexDashboard(Dashboard):
	"""
	Custom Grappelli index dashboard
	"""

	def init_with_context(self, context):
		self.columns = 3

		self.children.append(modules.Group(
			_("Products"),
			column=1,
			children=[
				modules.AppList(
					_("Products"),
					models=(
						'boilerplate.base.models.Product',
					),
				),
			]
		))

		self.children.append(modules.Group(
			_("Extras"),
			column=2,
			children=[
				modules.AppList(
					_("Pages"),
					models=('lemoulin_extras.pages.models.*', ),
				),
				modules.AppList(
					_("Extra Context"),
					models=('lemoulin_extras.dynamic_settings.models.*', ),
				),
			]
		))

		self.children.append(modules.Group(
			_("Système"),
			column=3,
			children=[
				modules.AppList(
					_("Accès"),
					models=('django.contrib.auth.*', ),
				),
				modules.AppList(
					_("Site"),
					models=('django.contrib.sites.*', ),
				),
			]
		))

		self.children.append(modules.LinkList(
			_('Cache'),
			column=3,
			collapsible=True,
			css_classes=('collapse closed',),
			children=(
				{
					'title': _("Clear cache"),
					'url': reverse("lemoulin_extras:cache:clear"),
					'external': False,
					'description': _("Clear all cached data"),
				},
			)
		))
