#!/bin/bash

NAME="boilerplate"										# Name of the application
VIRTUALENVDIR=/srv/.virtualenvs/boilerplate			# Virtualenv directory
DJANGODIR=/srv/www/boilerplate/						# Django project directory
SOCKFILE=/srv/www/boilerplate/run/gunicorn_boilerplate.sock	# we will communicte using this unix socket
USER=www-data									# the user to run as
GROUP=www-data									# the group to run as
NUM_WORKERS=3									# how many worker processes should Gunicorn spawn
TIMEOUT=30										# Worker timeout
DJANGO_SETTINGS_MODULE=boilerplate.conf.settings		# which settings file should Django use
DJANGO_WSGI_MODULE=boilerplate.wsgi				# WSGI module name

echo "Starting $NAME"

# Activate the virtual environment
#cd $DJANGODIR
source $VIRTUALENVDIR/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec $VIRTUALENVDIR/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
	--name $NAME \
	--workers $NUM_WORKERS \
	--timeout $TIMEOUT \
	--user=$USER --group=$GROUP \
	--log-level=info \
	--bind=unix:$SOCKFILE
	--reload
