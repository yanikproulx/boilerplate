#!/bin/bash

# Script to set up dependencies for Django on Vagrant.

PGSQL_VERSION=9.3

PROJECT_NAME=$1

# variable
PROJECT_DIR=/vagrant
SERVER_ROOT=/srv
SITES_ROOT=$SERVER_ROOT/www
VIRTUALENVS_DIR=$SERVER_ROOT/.virtualenvs
VIRTUALENV_DIR=$VIRTUALENVS_DIR/$PROJECT_NAME
NGINX_DIR=$SITES_ROOT/$PROJECT_NAME

# create server root directory
su - vagrant -c "sudo mkdir -p $SITES_ROOT"
su - vagrant -c "sudo mkdir -p $VIRTUALENVS_DIR"

# set ownership
su - vagrant -c "sudo chown -R vagrant:vagrant $SERVER_ROOT"

# create symlink from project dir to nginx dir
su - vagrant -c "ln -s /vagrant $NGINX_DIR"

# media and static
mkdir -m 777 $PROJECT_DIR/media
mkdir -m 777 $PROJECT_DIR/static/CACHE

# copy bashrc
cp -p $PROJECT_DIR/vagrant_data/bashrc /home/vagrant/.bashrc
cp -p $PROJECT_DIR/vagrant_data/etc-bash.bashrc /etc/bash.bashrc

# Need to fix locale so that Postgres creates databases in UTF-8
locale-gen en_US.UTF-8
dpkg-reconfigure locales

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# # # Install essential packages from Apt
apt-get update -y
apt-get upgrade

# Python dev packages
apt-get install -y zsh git-core vim-common sendmail python-dev python3-dev libjpeg-dev zlib1g-dev libfreetype6 libfreetype6-dev

# Git (we'd rather avoid people keeping credentials for git commits in the repo, but sometimes we need it for pip requirements that aren't in PyPI)
apt-get install -y git

# Postgresql
if ! command -v psql; then
    apt-get install -y postgresql-$PGSQL_VERSION libpq-dev
    cp $PROJECT_DIR/vagrant_data/pg_hba.conf /etc/postgresql/$PGSQL_VERSION/main/
    /etc/init.d/postgresql reload
fi

# # # easyinstall, pip
apt-get install -y python-setuptools
easy_install pip

pip install virtualenv
pip install virtualenvwrapper

# virtualenv setup for project
su - vagrant -c "mkdir -p /home/vagrant/.pip_download_cache"

# create virtualenv using project's name
su - vagrant -c "/usr/local/bin/virtualenv --no-site-packages --python=/usr/bin/python3 $VIRTUALENV_DIR"

# activate virtualenv
source $VIRTUALENV_DIR/bin/activate

# install lemouilin-utils first
pip install git+https://bitbucket.org/lemoulin/django-lemoulin-utils

# then install all requirements in correct order
cat $PROJECT_DIR/requirements.txt | xargs -n 1 $VIRTUALENV_DIR/bin/pip install

# copy local setting file
su - vagrant -c "cp $PROJECT_DIR/$PROJECT_NAME/conf/samples/settings_local.py $PROJECT_DIR/$PROJECT_NAME/conf/settings_local.py"

# # supervisor nginx nodejs npm
apt-get install -y nodejs npm

# symlink for nodejs
ln -s /usr/bin/nodejs /usr/bin/node

# CoffeeScript and LESS
npm install -g less
npm install -g coffee-script
npm install -g bower
npm install -g gulp
npm install -g gulp-less

# create project db
$PROJECT_DIR/manage.py sqlcreate | psql -U postgres

# Cleanup
apt-get clean


# ------------------------
# FOR THE FUTURE
# ------------------------
# run director permissions
#chmod 777 $PROJECT_DIR/run

# nginx config
#su - vagrant -c "sudo cp $PROJECT_DIR/docs/nginx.conf.sample /etc/nginx/sites-available/$PROJECT_NAME"
#su - vagrant -c "sudo ln -s /etc/nginx/sites-available/$PROJECT_NAME /etc/nginx/sites-enabled/$PROJECT_NAME"
#su - vagrant -c "sudo /etc/init.d/nginx restart"

# start.bash
#su - vagrant -c "cp $PROJECT_DIR/bin/samples/start.bash $PROJECT_DIR/bin/start.bash"
#su - vagrant -c "sudo chmod 775 $PROJECT_DIR/bin/start.bash"

# copy conf and restart Supervisor
#su - vagrant -c "sudo cp $PROJECT_DIR/docs/supervisor.conf.sample /etc/supervisor/conf.d/$PROJECT_NAME.conf"
#su - vagrant -c "sudo /etc/init.d/supervisor restart"

