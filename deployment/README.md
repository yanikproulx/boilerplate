# Using Ansible #

This site uses ansible for server provisioning and deployments.

## settings ##

The first thing to do is to configure your local settings. For other environments (production, stage), settings should already be provided.

Define your local server(s):

Copy `inventories/local.yml.sample` to `inventories/local.yml.sample` and edit according to your environment.

Define your local settings:

Copy `group_vars/local.yml.sample` to `group_vars/local.yml.sample` and edit according to your environment.

Other environment-specific files, such as `group_vars/production.yml` are encrypted using ansible-vault so as not to share passwords and secure information.

## Commands ##

### Update app ###

This is used for updating the app. It will update the code from teh git repos and run the post-deploy script.

`ansible-playbook -i inventories/[environment].yml update_app.yml`
