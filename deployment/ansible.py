#!/usr/bin/env python3

import argparse

defaults = {
	"vault_file": "vault_pass.txt",
	"extra_vars": None,
	"ask-sudo-pass": True,
}

#valid_commands = ["encrypt", "decrypt", "provision", "initproject", "deploy", ]
valid_commands = ["provision", "initproject", "deploy", ]

parser = argparse.ArgumentParser(
	description="Shortcut to run an ansible playbook",
)

parser.add_argument(
	"command",
	help="command to run [{valid_commands}]".format(valid_commands=", ".join(valid_commands))
)

#parser.add_argument(
#	"--ask-sudo-pass",
#	help="asks for sudo password before execution",
#	action="store_true",
#)

parser.add_argument(
	"host",
	help="host on wich to run (local, stage, production, etc.)",
)

parser.add_argument(
	"-v", "--verbose",
	help="displays verbose output",
	action="count",
	default=0,
)

parser.add_argument(
	"--vault_file",
	help="vault-file for encrypted files (default: {vault_file})".format(vault_file=defaults["vault_file"]),
	default=defaults["vault_file"],
)

parser.add_argument(
	"--update_requirements",
	help="update requirements (used on deploy)",
	action="store_true",
)

parser.add_argument(
	"--update_settings",
	help="update settings file (used on deploy)",
	action="store_true",
)


args = parser.parse_args()

ansible_command = "ansible-playbook -i inventories/{host} {command}.yml".format(host=args.host, command=args.command)

if args.vault_file:
	ansible_command = "{ansible_command} --vault-password-file={vault_file}".format(ansible_command=ansible_command, vault_file=args.vault_file)

extra_args = {}

if args.update_requirements:
	extra_args["update_requirements"] = "true"

if args.update_settings:
	extra_args["update_settings"] = "true"

#["{key}={val}".format(key=key, val=val) for key,val in extra_args.items()]

print(ansible_command)
