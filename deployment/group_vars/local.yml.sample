---
# file: group_vars/local.yml

app_name: "nationalnoel"

# system settings
system:
  os:
    family: "ubuntu"
    name: "ubuntu"
    version: 14.04
    package_manager: "apt"
  selinux:
    state: "disabled"
  sudoers:
    group: "sudo"
  access:
    # Set value here
    deploy_user: "deploy"

webserver:
  python:
    path: "/usr/bin/python3"

  virtualenv:
    base_virtualenvs_path: "/srv/.virtualenvs"
    path: "/srv/.virtualenvs/{{ app_name }}"
    virtualenvwrapper_path: "/usr/local/bin/virtualenvwrapper.sh"

# webservers settings
app:
  socket:
    file: "/srv/www/{{ app_name }}/run/gunicorn.sock"

  domain:
    # Set value here
    host: "iq.lemoulin.co"

  base_path: "/srv/www/{{ app_name }}"

  requirements:
    file: "/srv/www/{{ app_name }}/requirements.txt"

  git:
    branch: "master"

# django settings
django:
  conf:
    do_syncdb: True
    do_migrate: False
    do_collectstatic: False
    settings_local_path: "{{ app.base_path }}/{{ project_name }}/conf/settings_local.py"
    post_deploy_script: "{{ app.base_path }}/bin/post_deploy.bash"
