var gulp         = require('gulp'),
	less         = require('gulp-less'),
	path         = require('path'),
	iconfont     = require('gulp-iconfont'),
	consolidate  = require('gulp-consolidate');
	runTimestamp = Math.round(Date.now()/1000);

function swallowError (error) {
	console.log(error.toString());
 	this.emit('end');
}

gulp.task('less', function() {
  return gulp.src('./static/css/main.less')
    .pipe(less())
	.on('error', swallowError) // prevent error to stop gulp process unless restart, better log output
    .pipe(gulp.dest('./static/css/'));
});

gulp.task('watch', function() {
	// watch any less file /css directory
	gulp.watch('static/css/**', ['less']);
});
